package com.example.testproject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;

import android.content.Context;
import android.os.AsyncTask;

public class RequestTask extends AsyncTask<String, Void, ArrayList<String>>{
	ProgressDialog dialog;
Context context;
public RequestTask(Context context) {
this.context = context;
}
protected void onPreExecute() {
	 dialog = ProgressDialog.show(context, "", 
            "Получаю данные", true);
}
	@Override
	protected ArrayList<String> doInBackground(String... params) {
		ArrayList<String> resul =new ArrayList<>();
		String URL = params[0];
		 HttpClient httpclient = new DefaultHttpClient();
		    HttpResponse response;
			try {
				response = httpclient.execute(new HttpGet(URL));
			
		    StatusLine statusLine = response.getStatusLine();
		    if(statusLine.getStatusCode() == HttpStatus.SC_OK){
		        ByteArrayOutputStream out = new ByteArrayOutputStream();
		        response.getEntity().writeTo(out);
		        out.close();
		        String responseString = out.toString();
		        JSONObject json = new JSONObject(responseString);
		        JSONArray jArray = json.getJSONArray("results");
		        for(int i=0; i< jArray.length(); i++){
		        	JSONObject jObject = jArray.getJSONObject(i);
		        	if(!jObject.isNull("formatted_address")){
		        		resul.add(jObject.getString("formatted_address"));
		        	}
		        	
		        }
		       
		    }
		        } catch (IOException e) {
		
				e.printStackTrace();
			} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		           
		       
		    
		return resul;
	}
	protected void onPostExecute(java.util.ArrayList<String> result) {
		dialog.dismiss();
	};
}