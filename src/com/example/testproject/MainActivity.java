package com.example.testproject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
ListView listView;
Button shearchButton;
EditText editText;
SharedPreferences preferences;
DBHelper dbHelper;
String queryString="";
static final String NAME_STRING = "query";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listView = (ListView)findViewById(R.id.lv_Items);
		shearchButton = (Button)findViewById(R.id.bt_Shearch);
		editText = (EditText)findViewById(R.id.et_Shearch);
		dbHelper = new DBHelper(this);
		preferences = getPreferences(MODE_PRIVATE);
		queryString = preferences.getString(NAME_STRING, "");
		if(queryString.length() >0)editText.setText(queryString);
		ReadDB();
	}
	public void OnShearchClick(View v) {
		RequestTask requestTask = new RequestTask(MainActivity.this);
		String localeString="";
		
		switch (Locale.getDefault().getLanguage()) {
		case"uk":
			localeString = "uk";
			break;
		case "ru":{
		localeString = "ru";
		}break;
		default:
			localeString = "en";
			break;
		}Locale.getDefault().getDisplayLanguage();
		requestTask.execute("http://maps.googleapis.com/maps/api/geocode/json?address="+editText.getText().toString()+"&language="+localeString);
	try {
		ArrayList<String> list = requestTask.get();
		if(list.size() >0 ){
		listView.setAdapter(new MyAdapter(list, MainActivity.this));
		WriteToDb(list);
		SavePrefs(editText.getText().toString());
		}
		else {
			Toast.makeText(MainActivity.this, "Не найдено города", Toast.LENGTH_LONG).show();
		}
	} catch (InterruptedException | ExecutionException e) {
		Log.i("inform_tag", e.getMessage());
	}
	}
	
	private void ReadDB() {
  SQLiteDatabase db = dbHelper.getWritableDatabase();
	Cursor cursor = db.query("mytable", null, null, null, null, null, null);
	ArrayList<String> strings = new ArrayList<>();
	while (cursor.moveToNext()) {
		strings.add(cursor.getString(cursor.getColumnIndex("adress")));
	}
	listView.setAdapter(new MyAdapter(strings, this));
	db.close();
	}
	private void WriteToDb(ArrayList<String> data){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.delete("mytable", null, null);
	for(String string: data){
		ContentValues values = new ContentValues();
		values.put("adress", string);
		db.insert("mytable", null, values);
	}
	db.close();
	}
	private void SavePrefs(String text){
		preferences = getPreferences(MODE_PRIVATE);
		    Editor ed = preferences.edit();
		    ed.putString(NAME_STRING,text);
		    ed.commit();
	}

	class MyAdapter extends BaseAdapter{
		ArrayList<String> array;
		Context context;
		public MyAdapter(ArrayList<String> list, Context context) {
			array = list;
			this.context = context;
		}
		@Override
		public int getCount() {return array.size();}
		@Override
		public Object getItem(int position) {return array.get(position);}
		@Override
		public long getItemId(int position) {return 0;}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null){
			convertView =	LayoutInflater.from(context).inflate(R.layout.list_element, null);
			}
			TextView textView = (TextView) convertView.findViewById(R.id.element_text);
			FrameLayout bacLayout = (FrameLayout)convertView.findViewById(R.id.background);
			if(position%2 == 0){
				textView.setTextColor(Color.WHITE);
				bacLayout.setBackgroundColor(Color.BLACK);				
			}
			textView.setText(array.get(position));
			return convertView;
		}
		
	}
	
}
